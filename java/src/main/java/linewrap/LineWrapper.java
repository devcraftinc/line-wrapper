package linewrap;

public class LineWrapper {

	public String wrap(String text, int max) {
		if (text.length() <= max)
			return text;
		int splitPoint = findSplitPoint(text, max);
		return text.substring(0, splitPoint) + "\n" + wrap(text.substring(splitPoint).trim(), max);
	}

	private int findSplitPoint(String text, int max) {
		int splitPoint = text.lastIndexOf(' ', max);
		if (splitPoint == -1)
			splitPoint = max;
		return splitPoint;
	}

}
