package linewrap;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class LineWrapperTest {

	@Test
	public void testWrap() throws Exception {
		assertThat(lineWrapper.wrap("", 1), is(""));
		assertThat(lineWrapper.wrap("X", 1), is("X"));
		assertThat(lineWrapper.wrap("XX", 1), is("X\nX"));
		assertThat(lineWrapper.wrap("X X", 1), is("X\nX"));
		assertThat(lineWrapper.wrap("XXX", 1), is("X\nX\nX"));
		assertThat(lineWrapper.wrap("X XX", 3), is("X\nXX"));
		assertThat(lineWrapper.wrap("X XX XXX", 3), is("X\nXX\nXXX"));
	}

	LineWrapper lineWrapper = new LineWrapper();

}
