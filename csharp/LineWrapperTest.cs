using System.Collections.Generic;
using Xunit;

namespace LineWrap.Tests
{
    public class LineWrapperTest
    {
        [Fact]
        public void ReturnTextIfTextFitsLineSize()
        {
            Assert.Equal("", Wrap("", 1));
            Assert.Equal("X", Wrap("X", 1));
        }

        [Fact]
        public void BreakTextAtLineSizeIfTextIsLongerThanLineSize()
        {
            Assert.Equal("X\nX", Wrap("XX", 1));
        }

        [Fact]
        public void HandleAllText()
        {
            Assert.Equal("X\nX\nX", Wrap("XXX", 1));
        }

        [Fact]
        public void RemoveLeadingSpaceAfterBreak()
        {
            Assert.Equal("X\nX", Wrap("X X", 1));
        }

        [Fact]
        public void BreakAtClosestSpace()
        {
            Assert.Equal("X\nXX", Wrap("X XX", 3));
        }

        private string Wrap(string text, int lineSize)
        {
            if (text.Length <= lineSize)
                return text;
            var splitPoint = FindSplitPoint(text, lineSize);
            return text.Substring(0, splitPoint) + "\n" + Wrap(text.Substring(splitPoint).TrimStart(), lineSize);
        }

        private static int FindSplitPoint(string text, int lineSize)
        {
            var splitPoint = text.LastIndexOf(' ', lineSize);
            if (splitPoint == -1)
                splitPoint = lineSize;
            return splitPoint;
        }
    }
}
